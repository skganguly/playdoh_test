import pandas as pd

f = 'shopee_sample.csv'
df = pd.read_csv(f)
col = df.columns
df.fillna(0)
mus = df['Monthly_units_sold'].sum()
sp = df['SalePrice'].sum() 

class MyClassAtONGIL:
    the_object = 'MyClassAtONGIL'

    def __init__(self, x, y):
        self.x = x # This is an instance variable x for the total monthly units sold. 
        self.y = y # This is an instance variable y for the total sale price. 

    def __str__(self):
        return 'The total monthly units sold is {:.2f}, and the total sale price is {:.2f}'.format(self.x,self.y)  

    def __repr__(self):
        return 'MyclassONGIL(x={},y={})'.format(self.x,self.y)

    def get_market_share(self): # The method to calculate the market share.
        if (int(self.y) > 0):
            return self.x / self.y
        else:
            return('Not a number')


m = MyClassAtONGIL(mus,sp)   # construct an instance

print(str(m))         # Invoke __str__(): 
print(repr(m))        # Invoke __repr__(): 
#print(m.x)
#print(m.y)
#print('The Market Share is')
#print(m.get_market_share())

